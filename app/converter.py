import argparse
import csv
import os.path
import re
from string import Template
from pathlib import Path

item_open = r'\item '
item_closed = r'\item[\done] '
key_id = r'%%id%%'
key_url = r'%%url%%'
key_title = r'%%title%%'
key_module = r'%%module%%'
key_scope = r'%%scope%%'
key_desc = r'%%description%%'
key_items = r'%%items%%'
key_criteria = r'%%criteria%%'

script_path = Path(__file__).parent

with open(os.path.join(script_path, 'resources', 'template_default.tex'), 'r') as f:
    template = f.read()

with open(os.path.join(script_path, 'resources', 'template_criteria_default.tex'), 'r') as f:
    template_criteria = f.read()

def compute_scope(entry):
    tags = str(entry['Labels'])
    return 'Optional' if 'optional' in tags.lower() else 'Mandatory'


def compute_module(entry):
    tags = str(entry['Labels']).lower()
    if 'documentation' in tags:
        return 'Documentation'
    match = re.search(r'module::(?P<module>\w+)', tags)

    module = str(match.group('module')) if match else "All"
    return module.capitalize()


def compute_description(entry):
    text = str(entry['Description'])
    return text.split(r'__AK__:')[0]


def compute_ref(gitlab_ref):
    id = int(gitlab_ref[1:])
    return Template(r'\hyperref[tab:r$id]{\texttt{\#}$id}').substitute(id=id)


def compute_refs(content):
    match = re.findall(r'(?P<id>#\d+)', content)
    result = content
    if match:
        for ref in match:
            result = result.replace(ref, compute_ref(ref))
    return result


def compute_item(line_item):
    match = re.search(r'^[*-] \[(?P<done>[ x])] (?P<content>.*)$', line_item)
    if not match:
        return None
    groups = match.groups()
    g1 = str(groups[0])
    done = g1 and g1.strip()
    content = compute_refs(str(groups[1]).strip())
    if content.endswith('.'):
        print("[WARN] item ends with punctuation: ", content)
    prefix = item_closed if done else item_open
    return prefix + content + '\n'


def compute_items(entry):
    text = str(entry['Description'])
    parts = text.split(r'__AK__:')
    if len(parts) != 2:
        return None
    raw_items = parts[1].split('\n')
    raw_items = filter(lambda x: x and x.strip, raw_items)
    built_items = map(compute_item, raw_items)
    built_items = filter(lambda x: x and x.strip, built_items)

    result = ''
    for item in built_items:
        result += str(item)

    return result if result else None


def create_requirement(entry) -> str:
    output = template
    output = output.replace(key_id, entry['Issue ID'])
    output = output.replace(key_url, entry['URL'])
    output = output.replace(key_title, entry['Title'])
    output = output.replace(key_scope, compute_scope(entry))
    output = output.replace(key_module, compute_module(entry))
    output = output.replace(key_desc, compute_description(entry))

    items = compute_items(entry)
    if items and items.strip():
        criteria = template_criteria.replace(key_items, items)
        output = output.replace(key_criteria, criteria)
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gitlab2tex converter', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', type=argparse.FileType('r'), help='Input CSV (from GitLab issues)')
    parser.add_argument('-o', type=argparse.FileType('w'), help='Output TeX file')
    parser.add_argument('-e', type=str, help='Comma separated list of issue-ids to exclude')
    args = vars(parser.parse_args())

    input_file = args['i']
    output_file = args['o']

    if 'e' in args:
        excluded = list(map(lambda s: str(s).strip(), str(args['e']).split(',')))
    else:
        excluded = []

    requirements = ''

    csv = csv.DictReader(input_file)
    for entry in csv:
        issueId = entry['Issue ID']
        if issueId not in excluded:
            print('[INFO] Working on', issueId)
            requirements += create_requirement(entry)

    output_file.write(requirements)
    output_file.close()
