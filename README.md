# `gitlab2latex` [![pipeline status](https://gitlab.com/martyschaer/gitlab2latex/badges/main/pipeline.svg)](https://gitlab.com/martyschaer/gitlab2latex/-/commits/main) 
Takes the CSV export of GitLab Issues and converts them into LaTeX for effortless use in a document.

This is mainly written because I need it, and thus may be overly specific.

## Usage
First run `python setup.py development`

You may wish to customize `app/resources/template_*`

To run the converter use: `python app/converter.py -i <your csv from gitlab> -o <your tex destination>`
