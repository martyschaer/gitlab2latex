import unittest

from app import converter


class TestBasicFeatures(unittest.TestCase):

    def test_description_empty(self):
        entry = {'Description': ''}
        result = converter.compute_description(entry)
        self.assertEqual(result, '')

    def test_description_no_description(self):
        entry = {'Description': '__AK__:\na bunch\nmore\nlines'}
        result = converter.compute_description(entry)
        self.assertEqual(result, '')

    def test_description_oneline(self):
        entry = {'Description': 'Hello\n__AK__:\na bunch\nmore\nlines'}
        result = converter.compute_description(entry)
        self.assertEqual(result, 'Hello\n')

    def test_description_muliline(self):
        entry = {'Description': 'Hello\nWorld\n__AK__:\na bunch\nmore\nlines'}
        result = converter.compute_description(entry)
        self.assertEqual(result, 'Hello\nWorld\n')

if __name__ == '__main__':
    unittest.main()