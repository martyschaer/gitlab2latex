import unittest

from app import converter


class TestBasicFeatures(unittest.TestCase):

    def test_scope_optional(self):
        entry = {'Labels': 'Optional, module::modulename, Progress'}
        result = converter.compute_scope(entry)
        self.assertEqual(result, 'Optional')

    def test_scope_mandatory(self):
        entry = {'Labels': 'module::modulename, Progress'}
        result = converter.compute_scope(entry)
        self.assertEqual(result, 'Mandatory')

    def test_scope_empty(self):
        entry = {'Labels': ''}
        result = converter.compute_scope(entry)
        self.assertEqual(result, 'Mandatory')

if __name__ == '__main__':
    unittest.main()