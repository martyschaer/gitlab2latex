import unittest

from app import converter


class TestBasicFeatures(unittest.TestCase):
    def test_refs_empty(self):
        content = ''
        result = converter.compute_refs(content)
        self.assertEqual(result, content)

    def test_refs_none(self):
        content = 'Hello, World'
        result = converter.compute_refs(content)
        self.assertEqual(result, content)

    def test_refs_single(self):
        content = 'see #3 for more'
        result = converter.compute_refs(content)
        self.assertEqual(result, r'see \hyperref[tab:r3]{\texttt{\#}3} for more')

    def test_refs_multiple(self):
        content = 'see #3 & #41 for more'
        result = converter.compute_refs(content)
        self.assertEqual(result, r'see \hyperref[tab:r3]{\texttt{\#}3} & \hyperref[tab:r41]{\texttt{\#}41} for more')

if __name__ == '__main__':
    unittest.main()