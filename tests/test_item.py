import unittest

from app import converter


class TestBasicFeatures(unittest.TestCase):

    def test_compute_item_malformed(self):
        line = '* This is some malfor}{][med garbage'
        result = converter.compute_item(line)
        self.assertIsNone(result)

    def test_compute_item_dash_open(self):
        line = r'- [ ] This is an Item'
        result = converter.compute_item(line)
        self.assertEqual(result.strip(), r'\item This is an Item')
        self.assertTrue(result.endswith('\n'))

    def test_compute_item_star_done(self):
        line = r'* [x] This is an Item'
        result = converter.compute_item(line)
        self.assertEqual(result.strip(), r'\item[\done] This is an Item')
        self.assertTrue(result.endswith('\n'))


    def test_compute_items_empty(self):
        entry = {'Description': ''}
        result = converter.compute_items(entry)
        self.assertIsNone(result)

    def test_compute_items_no_items(self):
        entry = {'Description': 'Some Line\n__AK__:\n'}
        result = converter.compute_items(entry)
        self.assertIsNone(result)

    def test_compute_items_malformed_item(self):
        entry = {'Description': 'Some Line\n__AK__:\nblablabla'}
        result = converter.compute_items(entry)
        self.assertIsNone(result)

    def test_compute_items(self):
        entry = {'Description': 'Some Line\n__AK__:\n- [ ] Point A\n* [x] Point B\n'}
        result = converter.compute_items(entry)
        self.assertEqual(result, '\\item Point A\n\\item[\\done] Point B\n')

if __name__ == '__main__':
    unittest.main()