import unittest

from app import converter


class TestBasicFeatures(unittest.TestCase):

    def test_module_single(self):
        entry = {'Labels': 'Optional, module::modulename, Progress'}
        result = converter.compute_module(entry)
        self.assertEqual(result, 'Modulename')

    def test_module_multiple(self):
        entry = {'Labels': 'module::modulename, Progress, module::secondmodule,'}
        result = converter.compute_module(entry)
        self.assertEqual(result, 'Modulename')

    def test_module_documentation(self):
        entry = {'Labels': 'Documentation, module::modulename, Progress, module::secondmodule,'}
        result = converter.compute_module(entry)
        self.assertEqual(result, 'Documentation')

if __name__ == '__main__':
    unittest.main()